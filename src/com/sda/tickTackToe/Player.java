package com.sda.tickTackToe;

import java.util.Scanner;

public class Player {
    String name;
    String symbol;

    public Player (String name, String symbol){
        this.name = name;
        this.symbol = symbol;
    }

}
