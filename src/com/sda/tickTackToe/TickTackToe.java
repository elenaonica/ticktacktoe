package com.sda.tickTackToe;

import java.util.Scanner;

public class TickTackToe {

    String[][] board;

    Player player1;
    Player player2;

    public TickTackToe(Player player1, Player player2){
        this.board = generateBoard();
        this.player1 = player1;
        this.player2 = player2;
    }

    public  String[][] generateBoard() {

        String[][] board = new String[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = "_";
            }
        }
        return board;

    }
    public void printBoard(){

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(this.board[i][j] + " ");
            }
            System.out.println();
        }
    }
    public void setSymbol (Player player){

        System.out.println(player.name + ", please enter your symbol: ");
        Scanner sc = new Scanner(System.in);
        String symbol = sc.next();
        while(!player.symbol.equals(symbol)){

            System.out.println("Your symbol is: " + player.symbol);
            symbol = sc.next();
        }
        System.out.println("Enter the row and the column of the position you want to place your symbol: ");
        Position symbolPosition = new Position (sc.nextInt(), sc.nextInt());

        symbolPosition.isPositionValid(sc,board);

        board[symbolPosition.row][symbolPosition.column] = symbol;
        printBoard();

    }


    public boolean isLineWinner(String symbol){

        boolean areEqual = true;
        boolean isWinner = false;
        for(int row = 0;row< 3;row++){
            for(int col = 0;col< 3;col++){

                if(this.board[row][col].equals(symbol)){
                    areEqual = areEqual && true;
                }
                else{
                    areEqual = false;
                }

            }

            if(areEqual){
                isWinner = true;
            }

            areEqual = true;



        }

        return isWinner;


    }

    public boolean isColumnWinner(String symbol){
        boolean isWinner = false;
        boolean areEqual = true;
        for(int col = 0;col<3;col++){
            for(int row = 0;row<3;row++){
                if(this.board[row][col].equals(symbol)){
                    areEqual = areEqual && true;
                }
                else{
                    areEqual = false;
                }
            }
            if(areEqual){
                isWinner = true;
                break;
            }
            areEqual = true;
        }

        return isWinner;

    }
    public boolean isMainDiagonalWinner(String symbol){
        boolean isWinner = false;
        boolean areEqual = true;
        for(int row = 0;row<3;row++){
            for(int col = 0;col<3;col++) {
                if (row == col) {
                    if (this.board[row][col].equals(symbol)) {
                        areEqual = areEqual && true;
                    } else {
                        areEqual = false;
                    }
                }
            }

        }
        if(areEqual){
            isWinner = true;
        }

        return isWinner;

    }
    public boolean isAntidiagonalWinner(String symbol){
        boolean isWinner = false;
        boolean areEqual = true;
        for(int row = 0;row<3;row++){
            for(int col = 0;col<3;col++) {
                if (col == 2 - row) {
                    if (this.board[row][col].equals(symbol)) {
                        areEqual = areEqual && true;
                    } else {
                        areEqual = false;
                    }
                }
            }

        }
        if(areEqual){
            isWinner = true;
        }

        return isWinner;

    }
    public boolean isPlayerWinner (Player player){

        boolean isWinner = false;

        boolean l = isLineWinner(player.symbol);
        boolean c = isColumnWinner(player.symbol);
        boolean d1 =  isMainDiagonalWinner(player.symbol);
        boolean d2 =  isAntidiagonalWinner(player.symbol);

        if(l || c || d1 || d2){
            isWinner = true;
        }
        return isWinner;
    }


    public boolean quitGame(){
        if(isPlayerWinner(this.player1)){
            System.out.println(player1.name + " won! Game over");
           return true;

        }
        else if(isPlayerWinner(this.player2)){
            System.out.println(player2.name  + " won! Game over");
            return true;
        }
        else if(isBoardFull()){
            System.out.println("Nobody won, Game over");
            return true;
        }
        else{
            return false;
        }

    }
    public boolean isBoardFull(){
        boolean isFull = true;
        for(int row = 0;row<3;row++) {
            for (int col = 0; col < 3; col++) {
                if(board[row][col].equals("_")){
                    isFull = false;
                    break;
                }
            }
        }
        return isFull;

            }

            public void play(){
                while (true) {

                    setSymbol(player1);
                    if (quitGame()) {
                        break;
                    }
                    setSymbol(player2);


                    if (quitGame()) {
                        break;
                    }
                }
            }
    }



