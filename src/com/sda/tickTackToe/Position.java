package com.sda.tickTackToe;

import java.util.Scanner;

public class Position {
    public int row;
    public int column;

    public Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    private void isPositionOccupied(Scanner sc, String[][] board) {

        while (!board[this.row][this.column].equals("_")) {
            System.out.println("The position is occupied, enter different row and column");
            this.row = sc.nextInt();
            this.column = sc.nextInt();

        }
    }

    private void isIndexInRange(Scanner sc, String[][] board) {

        while (this.row > 2 || this.column > 2) {
            System.out.println("Enter row or column in range 0 -" + 2);
            this.row = sc.nextInt();
            this.column = sc.nextInt();
        }

    }

    public void isPositionValid(Scanner sc, String[][] board) {

        isIndexInRange(sc, board);
        isPositionOccupied(sc, board);
    }

}
