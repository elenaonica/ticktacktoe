package com.sda.tickTackToe;

public class Main {

    public static void main(String[] args) {


        Player player1 = new Player("Evan", "x");
        Player player2 = new Player("Elena", "0");
        TickTackToe tickTackToe = new TickTackToe(player1, player2);

        tickTackToe.printBoard();
        tickTackToe.play();



    }
}
